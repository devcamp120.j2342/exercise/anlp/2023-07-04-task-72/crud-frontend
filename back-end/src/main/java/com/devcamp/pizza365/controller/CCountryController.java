package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.IRegionRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CCountryController {
	@Autowired
	private CountryRepository countryRepository;

	@Autowired
	IRegionRepository iRegionRepository;

	@GetMapping("/countries")
	public ResponseEntity<List<CCountry>> getDrinkList() {
		try {
			List<CCountry> pDrinkLists = new ArrayList<CCountry>();

			countryRepository.findAll().forEach(pDrinkLists::add);

			return new ResponseEntity<>(pDrinkLists, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/countries")
	public ResponseEntity<Object> createCountry(@RequestBody CCountry cCountry) {
		try {
			CCountry newRole = new CCountry();
			newRole.setCountryName(cCountry.getCountryName());
			newRole.setCountryCode(cCountry.getCountryCode());
			newRole.setRegions(cCountry.getRegions());
			CCountry savedRole = countryRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/countries/{id}")
	public ResponseEntity<Object> updateCountry(@PathVariable("id") Long id, @RequestBody CCountry cCountry) {
		Optional<CCountry> countryData = countryRepository.findById(id);
		if (countryData.isPresent()) {
			CCountry newCountry = countryData.get();
			cCountry.setId(newCountry.getId());
			CCountry savedCountry = countryRepository.save(cCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/countries/{id}")
	public ResponseEntity<Object> deleteCountryById(@PathVariable Long id) {
		try {
			countryRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/countries/{id}")
	public CCountry getCountryById(@PathVariable Long id) {
		if (countryRepository.findById(id).isPresent())
			return countryRepository.findById(id).get();
		else
			return null;
	}

	@Autowired
	private IRegionRepository regionRepository;
	
	@GetMapping("/countries/{countryId}/regions")
	public List<CRegion> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.findByCountryId(countryId);
	}

	@GetMapping("/countries/{countryCode}/{regionCode}")
	public Optional<CRegion> getRegionByCountryCodeAndRegionCode(
			@PathVariable(value = "countryCode") String countryCode,
			@PathVariable(value = "regionCode") String regionCode) {
		Optional<CRegion> optional = regionRepository.findByRegionCodeAndCountryCountryCode(regionCode, countryCode);
		if (optional.isPresent()) {
			return optional;
		} else {
			return null;
		}
	}

	@GetMapping("/regions/code/{regionCode}")
	public CRegion getRegionByCode(@PathVariable(value = "regionCode") String regionCode) {
		return regionRepository.findByRegionCode(regionCode);
	}

	@GetMapping("/regions")
	public ResponseEntity<List<CRegion>> getAllVouchers() {
		try {
			List<CRegion> pRegions = new ArrayList<CRegion>();

			iRegionRepository.findAll().forEach(pRegions::add);

			return new ResponseEntity<>(pRegions, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/regions/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}

	@PostMapping("/countries/{countryId}/regions")
	public ResponseEntity<Object> createCRegion(@PathVariable(value = "countryId") Long countryId,
			@RequestBody CRegion pRegions) {// Sửa để cho phép validate
		Optional<CCountry> countryData = countryRepository.findById(countryId);
		if (countryData.isPresent()) {
			CCountry vCountry = countryData.get();
			List<CRegion> newRegions = new ArrayList<CRegion>();
			newRegions = vCountry.getRegions();
			newRegions.add(pRegions);
			vCountry.setRegions(newRegions);
			CCountry savedCountry = countryRepository.save(vCountry);
			return new ResponseEntity<>(savedCountry, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PutMapping("/regions/{id}")
	public ResponseEntity<Object> updateCRegionById(@PathVariable("id") long id, @RequestBody CRegion pRegions) {
		Optional<CRegion> regionData = iRegionRepository.findById(id);
		if (regionData.isPresent()) {
			CRegion region = regionData.get();
			region.setRegionCode(pRegions.getRegionCode());
			region.setRegionName(pRegions.getRegionName());
			try {
				return new ResponseEntity<>(iRegionRepository.save(region), HttpStatus.OK);
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified Voucher:" + e.getCause().getCause().getMessage());
			}

		} else {
			// return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified Voucher: " + id + "  for update.");
		}
	}

	// @DeleteMapping("/countries/{countryId}/regions")
	// public ResponseEntity<CRegion> deleteAllRegionsByCountryId(@PathVariable(value = "countryId") Long countryId) {
	// 	try {
	// 		iRegionRepository.deleteAllRegionsByCountryId(countryId);
	// 		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	// 	} catch (Exception e) {
	// 		System.out.println(e);
	// 		return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
	// 	}
	// }

	@DeleteMapping("/regions/{regionId}")
	public ResponseEntity<CRegion> deleteRegionsById(@PathVariable(value = "regionId") Long regionId) {
		try {
			iRegionRepository.deleteById(regionId);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/regions")
	public ResponseEntity<CRegion> deleteAllCRegion() {
		try {
			iRegionRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
