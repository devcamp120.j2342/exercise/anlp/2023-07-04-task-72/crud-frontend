package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.repository.IDrinkRepository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
	@Autowired 
	IDrinkRepository pDrinkRepository;
	
	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getDrinkList(){
		try {
			List<CDrink> pDrinkLists = new ArrayList<CDrink>();
			
			pDrinkRepository.findAll().forEach(pDrinkLists::add);
			
			return new ResponseEntity<>(pDrinkLists, HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/drinks/{id}")
	public ResponseEntity<CDrink> getCDrinkById(@PathVariable("id") long id) {
		Optional<CDrink> menuData = pDrinkRepository.findById(id);
		if (menuData.isPresent()) {
			return new ResponseEntity<>(menuData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/drinks")
	public ResponseEntity<Object> createCDrink( @RequestBody CDrink pDrinks) {//Sửa để cho phép validate
		try {
			pDrinks.setNgayTao(new Date());
			pDrinks.setNgayCapNhat(null);
			CDrink _drinks = pDrinkRepository.save(pDrinks);
			return new ResponseEntity<>(_drinks, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified menu: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/drinks/{id}")
	public ResponseEntity<Object> updateCDrinkById(@PathVariable("id") long id, @RequestBody CDrink pDrinks) {
		Optional<CDrink> menuData = pDrinkRepository.findById(id);
		if (menuData.isPresent()) {
			CDrink menu = menuData.get();
			menu.setMaNuocUong(pDrinks.getMaNuocUong());
			menu.setTenNuocUong(pDrinks.getTenNuocUong());
			menu.setDonGia(pDrinks.getDonGia());
			menu.setNgayCapNhat(new Date());
			try {
				return new ResponseEntity<>(pDrinkRepository.save(menu), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified menu:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified menu: "+id + "  for update.");
		}
	}

	@DeleteMapping("/drinks/{id}")
	public ResponseEntity<CDrink> deleteCDrinkById(@PathVariable("id") long id) {
		try {
			pDrinkRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/drinks")
	public ResponseEntity<CDrink> deleteAllCDrink() {
		try {
			pDrinkRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
