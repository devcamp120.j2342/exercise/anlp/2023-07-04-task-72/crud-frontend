package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.repository.IMenuRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CMenuController {
	@Autowired
	IMenuRepository pMenuRepository;
	
	@GetMapping("/menus")
	public ResponseEntity<List<CMenu>> getAllMenu(){
		try {
			List<CMenu> pMenuList = new ArrayList<CMenu>();
			
			pMenuRepository.findAll().forEach(pMenuList::add);
			
			return new ResponseEntity<>(pMenuList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/menus/{id}")
	public ResponseEntity<CMenu> getCMenuById(@PathVariable("id") long id) {
		Optional<CMenu> menuData = pMenuRepository.findById(id);
		if (menuData.isPresent()) {
			return new ResponseEntity<>(menuData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/menus")
	public ResponseEntity<Object> createCMenu( @RequestBody CMenu pmenus) {//Sửa để cho phép validate
		try {
			CMenu _menus = pMenuRepository.save(pmenus);
			return new ResponseEntity<>(_menus, HttpStatus.CREATED);
		}catch (Exception  e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			//Hiện thông báo lỗi tra back-end
			return new ResponseEntity<>(e.getCause().getCause().getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
			//return ResponseEntity.unprocessableEntity().body("Failed to Create specified menu: "+e.getCause().getCause().getMessage());
		}
	}

	@PutMapping("/menus/{id}")
	public ResponseEntity<Object> updateCMenuById(@PathVariable("id") long id, @RequestBody CMenu pmenus) {
		Optional<CMenu> menuData = pMenuRepository.findById(id);
		if (menuData.isPresent()) {
			CMenu menu = menuData.get();
			menu.setSize(pmenus.getSize());
			menu.setDuongKinh(pmenus.getDuongKinh());
			menu.setSuon(pmenus.getSuon());
			menu.setSalad(pmenus.getSalad());
			menu.setSoLuongNuocNgot(pmenus.getSoLuongNuocNgot());
			menu.setDonGia(pmenus.getDonGia());
			try {
				return new ResponseEntity<>(pMenuRepository.save(menu), HttpStatus.OK);	
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity().body("Failed to Update specified menu:"+e.getCause().getCause().getMessage());
			}
			
		} else {
			//return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			return ResponseEntity.badRequest().body("Failed to get specified menu: "+id + "  for update.");
		}
	}

	@DeleteMapping("/menus/{id}")
	public ResponseEntity<CMenu> deleteCMenuById(@PathVariable("id") long id) {
		try {
			pMenuRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/menus")
	public ResponseEntity<CMenu> deleteAllCMenu() {
		try {
			pMenuRepository.deleteAll();
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
