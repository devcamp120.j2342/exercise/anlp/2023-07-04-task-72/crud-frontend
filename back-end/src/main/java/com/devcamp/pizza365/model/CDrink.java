package com.devcamp.pizza365.model;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;


@Entity
@Table(name = "p_drinks")
public class CDrink {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long stt;
	
	@Column(name = "ma_nuoc_uong")
	private String maNuocUong;
	
	@Column(name = "ten_nuoc_uong")
	private String tenNuocUong;
	
	@Column(name = "don_gia")
	private int donGia;
	
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayTao;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_cap_nhat", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayCapNhat;
	
	public CDrink() {
		
	}
	
	public CDrink(String maNuocUong, String tenNuocUong, int donGia, Date ngay_tao, Date ngay_cap_nhat) {
		this.maNuocUong = maNuocUong;
		this.tenNuocUong = tenNuocUong;
		this.donGia = donGia;
		this.ngayTao = ngay_tao;
		this.ngayCapNhat = ngay_cap_nhat;
	}
	
	public long getStt() {
		return stt;
	}
	public void setStt(int stt) {
		this.stt = stt;
	}
	public String getMaNuocUong() {
		return maNuocUong;
	}
	public void setMaNuocUong(String maNuocUong) {
		this.maNuocUong = maNuocUong;
	}
	public String getTenNuocUong() {
		return tenNuocUong;
	}
	public void setTenNuocUong(String tenNuocUong) {
		this.tenNuocUong = tenNuocUong;
	}
	public int getDonGia() {
		return donGia;
	}
	public void setDonGia(int donGia) {
		this.donGia = donGia;
	}
    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    public Date getNgayCapNhat() {
        return ngayCapNhat;
    }

    public void setNgayCapNhat(Date ngayCapNhat) {
        this.ngayCapNhat = ngayCapNhat;
    }
	
}
