"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gmenuTable = $("#menu-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'size' },
        { data: 'duongKinh' },
        { data: 'suon' },
        { data: 'salad' },
        { data: 'soLuongNuocNgot' },
        { data: 'donGia' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#menu-table").on("click", ".fa-edit", function () {
        onDeatilMenuClick(this); // this là button được ấn
    });
    $("#menu-table").on("click", ".fa-trash", function () {
        onDeleteMenuByIdClick(this); // this là button được ấn
    });
    $("#create-menu").on("click", function () {
        onCreateNewMenuClick();
    });
    $("#update-menu").on("click", function () {
        onSaveMenuClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-menu').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-menu').on("click", function () {
        $('#modal-delete-all-menu').modal('show');
    });
    $("#confirm-delete-all-menu").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "menus",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            console.log(responseObject)
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilMenuClick(paramChiTietButton) {
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gmenuTable.row(vRowSelected).data();
    gId = vDatatableRow.menu_id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "menus/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadmenuToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteMenuByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-menu').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gmenuTable.row(vRowSelected).data();
    gId = vDatatableRow.menu_id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "menus/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-menu').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "menu with id: " + gId + " was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "menus",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveMenuClick() {
    var newMenu = {
        size: $('#input-size').val().trim(),
        duongKinh: $('#input-duong-kinh').val().trim(),
        suon: $('#input-suon').val().trim(),
        salad: $('#input-salad').val().trim(),
        soLuongNuocNgot: $('#input-so-luong-nuoc-ngot').val().trim(),
        donGia: $('#input-don-gia').val().trim(),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "menus/" + gId,
        method: 'PUT',
        data: JSON.stringify(newMenu),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetMenuInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewMenuClick() {
    var newMenu = {
        size: $('#input-size').val().trim(),
        duongKinh: $('#input-duong-kinh').val().trim(),
        suon: $('#input-suon').val().trim(),
        salad: $('#input-salad').val().trim(),
        soLuongNuocNgot: $('#input-so-luong-nuoc-ngot').val().trim(),
        donGia: $('#input-don-gia').val().trim(),
    };
    if (validateMenu(newMenu)) {
        $.ajax({
            url: gBASE_URL + '/menus',
            method: 'POST',
            data: JSON.stringify(newMenu),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "menu created successfully ",
                })
                onPageLoading();
                resetMenuInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gmenuTable.clear();
    //Cập nhật data cho bảng 
    gmenuTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gmenuTable.draw();
}
// load dữ liệu vào form
function loadmenuToInput(parammenu) {
    $('#input-size').val(parammenu.size);
    $('#input-duong-kinh').val(parammenu.duongKinh);
    $('#input-suon').val(parammenu.suon);
    $('#input-salad').val(parammenu.salad);
    $('#input-so-luong-nuoc-ngot').val(parammenu.soLuongNuocNgot);
    $('#input-don-gia').val(parammenu.donGia);
}
function resetMenuInput() {
    $('#input-size').val('');
    $('#input-duong-kinh').val('');
    $('#input-suon').val('');
    $('#input-salad').val('');
    $('#input-so-luong-nuoc-ngot').val('');
    $('#input-don-gia').val('');
}

function validateMenu(paramObj) {
    if (paramObj.size == "") {
        toastCreateOrder("cỡ pizza")
        return false;
    } else if (paramObj.duongKinh <= 0) {
        toastCreateOrder("đường kính")
        return false;
    } else if (paramObj.suon <= 0) {
        toastCreateOrder("sườn nướng")
        return false;
    } else if (paramObj.salad <= 0) {
        toastCreateOrder("salad")
        return false;
    } else if (paramObj.soLuongNuocNgot <= 0) {
        toastCreateOrder("số lượng nước ngọt")
        return false;
    } else if (paramObj.donGia <= 0) {
        toastCreateOrder("đơn giá")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}