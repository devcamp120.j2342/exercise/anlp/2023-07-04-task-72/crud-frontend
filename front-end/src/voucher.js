"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gVoucherTable = $("#voucher-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'maVoucher' },
        { data: 'phanTramGiamGia' },
        { data: 'ghiChu' },
        { data: 'ngayTao' },
        { data: 'ngayCapNhat' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function() {
              return stt++;
            }
          },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#voucher-table").on("click", ".fa-edit", function () {
        onDeatilVoucherClick(this); // this là button được ấn
    });
    $("#voucher-table").on("click", ".fa-trash", function () {
        onDeleteVoucherByIdClick(this); // this là button được ấn
    });
    $("#create-voucher").on("click", function () {
        onCreateNewVoucherClick();
    });
    $("#update-voucher").on("click", function () {
        onSavevoucherClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-voucher').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-voucher').on("click", function () {
        $('#modal-delete-all-voucher').modal('show');
    });
    $("#confirm-delete-all-voucher").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "vouchers",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilVoucherClick(paramChiTietButton) {
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gVoucherTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "vouchers/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadVoucherToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteVoucherByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-voucher').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gVoucherTable.row(vRowSelected).data();
    gId = vDatatableRow.id;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "vouchers/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-voucher').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "voucher with id: "+ gId +" was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "vouchers",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSavevoucherClick() {
    var newVoucher = {
        maVoucher: $('#input-ma-voucher').val().trim(),
        phanTramGiamGia: $('#input-phan-tram-giam-gia').val().trim(),
        ghiChu: $('#input-ghi-chu').val().trim(),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "vouchers/" + gId,
        method: 'PUT',
        data: JSON.stringify(newVoucher),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetVoucherInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewVoucherClick() {
    var newVoucher = {
        maVoucher: $('#input-ma-voucher').val().trim(),
        phanTramGiamGia: $('#input-phan-tram-giam-gia').val().trim(),
        ghiChu: $('#input-ghi-chu').val().trim(),
    };
    if (validateVoucher(newVoucher)) {
        $.ajax({
            url: gBASE_URL + '/vouchers',
            method: 'POST',
            data: JSON.stringify(newVoucher),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "voucher created successfully ",
                })
                onPageLoading();
                resetVoucherInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gVoucherTable.clear();
    //Cập nhật data cho bảng 
    gVoucherTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gVoucherTable.draw();
}
// load dữ liệu vào form
function loadVoucherToInput(paramVoucher) {
    $('#input-ma-voucher').val(paramVoucher.maVoucher);
    $('#input-phan-tram-giam-gia').val(paramVoucher.phanTramGiamGia);
    $('#input-ghi-chu').val(paramVoucher.ghiChu);
}
function resetVoucherInput() {
    $('#input-ma-voucher').val('');
    $('#input-phan-tram-giam-gia').val('');
    $('#input-ghi-chu').val('');
}

function validateVoucher(paramObj) {
    if (paramObj.maVoucher == "") {
        toastCreateOrder("mã voucher")
        return false;
    } else if (paramObj.phanTramGiamGia <= 0) {
        toastCreateOrder("phần trăm giảm giá")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}