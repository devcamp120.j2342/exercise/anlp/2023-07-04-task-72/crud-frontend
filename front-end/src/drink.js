"use strict";
console.log("load page")
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://127.0.0.1:8080/";
var stt = 1;
// định nghĩa table  - chưa có data
var gDrinkTable = $("#drink-table").DataTable({
    // Khai báo các cột của datatable
    "columns": [
        { data: 'STT' },
        { data: 'maNuocUong' },
        { data: 'tenNuocUong' },
        { data: 'donGia' },
        { data: 'ngayTao' },
        { data: 'ngayCapNhat' },
        { data: 'action' },
    ],
    columnDefs: [
        {
            targets: 0, // Cột số thứ tự là 0 (cột STT)
            render: function () {
                return stt++;
            }
        },
        {
            targets: -1,
            defaultContent: `<i class="fas fa-edit text-primary"></i>
        | <i class="fas fa-trash text-danger"></i>`,
        },
    ],
});
var gId = -1;
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    // gán click event handler cho button chi tiet
    $("#drink-table").on("click", ".fa-edit", function () {
        onDeatilDrinkClick(this); // this là button được ấn
    });
    $("#drink-table").on("click", ".fa-trash", function () {
        onDeleteDrinkByIdClick(this); // this là button được ấn
    });
    $("#create-drink").on("click", function () {
        onCreateNewDrinkClick();
    });
    $("#update-drink").on("click", function () {
        onSaveDrinkClick();
    });
    // khi xác nhận xóa trên modal delete
    $('#delete-drink').on("click", function () {
        onConfirmDeleteClick();
    });
    // xóa toàn bộ phòng ban
    $('#delete-all-drink').on("click", function () {
        $('#modal-delete-all-drink').modal('show');
    });
    $("#confirm-delete-all-drink").on("click", function () {
        onConfirmDeleteAllClick();
    });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm thực hiện khi load trang
function onPageLoading() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "drinks",
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loadDataToTable(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
    stt = 1;
}
// Hàm xử lý khi ấn nút chi tiết
function onDeatilDrinkClick(paramChiTietButton) {
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gDrinkTable.row(vRowSelected).data();
    gId = vDatatableRow.stt;
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "drinks/" + gId,
        type: "GET",
        dataType: 'json',
        success: function (responseObject) {
            loaddrinkToInput(responseObject);
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
// Hàm xử lý khi ấn nút xóa trên bảng
function onDeleteDrinkByIdClick(paramChiTietButton) {
    // hiện modal
    $('#modal-delete-drink').modal('show');
    //Xác định thẻ tr là cha của nút được chọn
    var vRowSelected = $(paramChiTietButton).parents('tr');
    //Lấy datatable row
    var vDatatableRow = gDrinkTable.row(vRowSelected).data();
    gId = vDatatableRow.stt;
}
function onConfirmDeleteClick() {
    $.ajax({
        url: gBASE_URL + "drinks/" + gId,
        method: 'DELETE',
        success: () => {
            $('#modal-delete-drink').modal('hide');
            Toast.fire({
                icon: 'success',
                title: "drink with id: " + gId + " was successfully deleted",
            })
            onPageLoading();
        },
        error: (err) => alert(err.responseText),
    });
}
function onConfirmDeleteAllClick() {
    // lấy data từ server
    $.ajax({
        url: gBASE_URL + "drinks",
        type: 'DELETE',
        success: function () {
            location.reload();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}

// hàm xử lí khi ấn cập nhật phòng ban
function onSaveDrinkClick() {
    var newDrink = {
        maNuocUong: $('#input-ma-nuoc-uong').val().trim(),
        tenNuocUong: $('#input-ten-nuoc-uong').val().trim(),
        donGia: $('#input-don-gia').val().trim(),
    };
    // đẩy data từ server
    $.ajax({
        url: gBASE_URL + "drinks/" + gId,
        method: 'PUT',
        data: JSON.stringify(newDrink),
        contentType: 'application/json',
        success: function () {
            onPageLoading();
            gId = 0;
            resetDrinkInput();
        },
        error: function (error) {
            console.assert(error.responseText);
        }
    });
}
function onCreateNewDrinkClick() {
    var newDrink = {
        maNuocUong: $('#input-ma-nuoc-uong').val().trim(),
        tenNuocUong: $('#input-ten-nuoc-uong').val().trim(),
        donGia: $('#input-don-gia').val().trim(),
    };
    if (validatedrink(newDrink)) {
        $.ajax({
            url: gBASE_URL + '/drinks',
            method: 'POST',
            data: JSON.stringify(newDrink),
            contentType: 'application/json',
            success: (data) => {
                Toast.fire({
                    icon: 'success',
                    title: "drink created successfully ",
                })
                onPageLoading();
                resetDrinkInput();
            },
            error: (err) => alert(err.responseText),
        });
    }
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// load data to table
function loadDataToTable(paramResponseObject) {
    //Xóa toàn bộ dữ liệu đang có của bảng
    gDrinkTable.clear();
    //Cập nhật data cho bảng 
    gDrinkTable.rows.add(paramResponseObject);
    //Cập nhật lại giao diện hiển thị bảng
    gDrinkTable.draw();
}
// load dữ liệu vào form
function loaddrinkToInput(paramdrink) {
    $('#input-ma-nuoc-uong').val(paramdrink.maNuocUong);
    $('#input-ten-nuoc-uong').val(paramdrink.tenNuocUong);
    $('#input-don-gia').val(paramdrink.donGia);
}
function resetDrinkInput() {
    $('#input-ma-nuoc-uong').val('');
    $('#input-ten-nuoc-uong').val('');
    $('#input-don-gia').val('');
}

function validatedrink(paramObj) {
    if (paramObj.maNuocUong == "") {
        toastCreateOrder("mã nước uống")
        return false;
    } else if (paramObj.phanTramGiamGia == "") {
        toastCreateOrder("tên nước uống")
        return false;
    } else if (paramObj.donGia <= 0 ) {
        toastCreateOrder("đơn giá")
        return false;
    }
    return true;
}

// tạo thông báo lỗi
var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
function toastCreateOrder(paramMessage) {
    Toast.fire({
        icon: 'warning',
        title: "Xin hãy nhập lại " + paramMessage,
    })
}